"""
Barium_utils implementation for salt

.. versionadded:: 2023.06.15
"""

import logging
import salt.utils.path

log = logging.getLogger(__name__)


def __virtual__():
    if salt.utils.path.which("barium"):
        return True
    return (False, "This is not Barium OS")


def _common(util, args):
    """
    common func
    """
    parameters = ''
    for a in args:
        parameters += f' {a}'
    barium_lst = []
    bariumcmd = __salt__["cmd.run"](
        "barium {} {}".format(util, parameters), python_shell=False
    ).splitlines()
    ret = {}
    for line in bariumcmd:
        if not line.startswith('\n'):
            barium_lst.append(line)
    ret['barium ' + util + parameters] = barium_lst
    return ret

def ls(*pars):
    """
    run: barium ls --raw
    """
    args = ['--raw', *pars]
    return _common('ls', args)


def update(*pars):
    """
    run: barium update -uv
         process can take a long time
    """
    args = ['-uv', *pars]
    return _common('update', args)

def update_modules(*pars):
    """
    run: barium update_modules
         process can take a long time
    """
    args = ['', *pars]
    return _common('update_modules', args)

def getmod(*pars):
    """
    run: barium getmod -f <module>
         <module> will be downloaded into /root dir.
    """
    args = ['-f', *pars]
    return _common('getmod', args)

def instmod(*pars):
    """
    run: barium instmod <module>
         <module> is local file or http,ftp,rsync 
    """
    args = ['', *pars]
    return _common('instmod', args)
